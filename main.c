#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZEP 10
#define SIZEC 10

struct parent {
    int key;
    char *info;
    struct parent *next;
};

struct child {
    int key;
    int pkey;
    char *info;
    struct child *next;
};

struct table {
    struct parent *par[SIZEP];
    struct child *ch[SIZEC];
    struct key *key[SIZEP];
};

struct key {  // таблиц для хранения ключей, клюом является родительский ключ
    int k;
    int pk;
    struct key *next;
};

int hash(int x) {
    x = ((x >> 16) ^ x) * 0x45d9f3b; 
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = (x >> 16) ^ x;
    return x;
}

const char *errmsgs[] = {"Ok",  //возможные ошибки
                         "Duplicate key",
                         "There is no such key in table",
                         "That's not number",
                         "Incorrect info",
                         "There is no such key in parent table",
                         "There is pkey in child table",
                         "Memory error"
};

const char *msgs[] = {"0. Quit",  //реализованные функции
                      "1. Add into parent table",
                      "2. Add into child table",
                      "3. Find in parent table",
                      "4. Find in child table",
                      "5. Delete in parent table",
                      "6. Delete in child table",
                      "7. Show parent table",
                      "8. Show child table"
};

const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

int getint(int *a) {
    int n;
    do {
        n = scanf("%d", a);
        if (n == 0 || a < 0) {
            scanf("%*[^\n]");
            printf("Error! That's not number\n");
        }
    }while (n == 0 || a < 0);
    scanf("%*c");
    return n<0?0:1;
}

char *getstr() { //функция безопасного получения строки
    char *ptr = (char *) malloc(1);  //строка
    char buf[100]; //буфер для получения из входного потока
    int n = 0;
    unsigned int len = 0;//len - длина строки
    *ptr = '\0';
    do {
        n = scanf("%99[^\n]", buf);
        if (n < 0) {
            free(ptr);
            ptr = NULL;
            continue;
        }
        if(n == 0)
            scanf("%*c");
        else {
            len += strlen(buf);
            ptr = (char *) realloc(ptr, len + 1);
            strcat(ptr, buf);
        }
    } while(n > 0);
    return ptr;
}

void printlistp(struct parent *ptr) { //печать одного из списков родительских элементов
    for(; ptr != NULL; ptr = ptr->next){
        printf("key %d: ", ptr->key);
        printf("%s", ptr->info);
        printf("\n");
    }
}

void printlistc(struct child *ptr) {//печать одного из списков детских элементов
    for(; ptr != NULL; ptr = ptr->next){
        printf("key %d and pkey %d: ", ptr->key, ptr->pkey);
        printf("%s", ptr->info);
        printf("\n");
    }
}

int insertp(struct parent *tab[], int key, char *info) { //вставка в родительскую таблицу указанного элемента
    struct parent *ptr = tab[hash(key) % SIZEP];
    struct parent *temp;
    if (ptr == NULL) {
        ptr = tab[hash(key) % SIZEP] = (struct parent *)malloc(sizeof(struct parent));
        if (ptr == NULL)
            return 7;
        ptr->key = key;
        ptr->info = info;
        ptr->next = NULL;
        return 0;
    }
    temp = tab[hash(key) % SIZEP] = (struct parent *)malloc(sizeof(struct parent));
    if (temp == NULL)
        return 7;
    temp->key = key;
    temp->info = info;
    temp->next = ptr;
    return 0;
}

int insertc(struct child *tab[], int key, int pkey, char *info) { ///вставка в детскую таблицу указанного элемента
    struct child *ptr = tab[hash(key) % SIZEC];
    struct child *temp;
    if (ptr == NULL) {
        ptr = tab[hash(key) % SIZEC] = (struct child *)malloc(sizeof(struct child));
        if (ptr == NULL)
            return 7;
        ptr->key = key;
        ptr->info = info;
        ptr->pkey = pkey;
        ptr->next = NULL;
        return 0;
    }
    temp = tab[hash(key) % SIZEC] = (struct child *)malloc(sizeof(struct child));
    if (temp == NULL)
        return 7;
    temp->key = key;
    temp->info = info;
    temp->pkey = pkey;
    temp->next = ptr;
    return 0;
}

int insertk(struct key *tab[], int key, int pkey) { //Вставка указанного ключа в таблицу ключей
    struct key *ptr = tab[hash(pkey) % SIZEP];
    struct key *temp;
    if (ptr == NULL) {
        ptr = tab[hash(pkey) % SIZEP] = (struct key *)malloc(sizeof(struct key));
        ptr->k = key;
        ptr->pk = pkey;
        ptr->next = NULL;
        return 0;
    }
    temp = tab[hash(pkey) % SIZEP] = (struct key *)malloc(sizeof(struct key));
    temp->k = key;
    temp->pk = pkey;
    temp->next = ptr;
    return 0;
}

void init(struct table *table) { //инициализация таблицы(заполняет все нулями)
    for (int i = 0; i < SIZEP; ++i)
        table->par[i] = NULL;
    for (int i = 0; i < SIZEC; ++i)
        table->ch[i] = NULL;
    for (int i = 0; i < SIZEP; ++i)
        table->key[i] = NULL;
}

struct parent *findp(struct parent *par[], int key) { //поиск указанного элемента в родительской таблице(возвращает сам элемент)
    struct parent *ptr = par[hash(key)%SIZEP];
    while (ptr != NULL) {
        if (ptr->key == key)
            return ptr;
        ptr = ptr->next;
    }
    return NULL; //при ненахождении возвращает нуль
}

struct child *findc(struct child *ch[], int key, int *pkey) { //поиск указанного элемента в детской таблице(возвращает сам элемент)
    struct child *ptr = ch[hash(key)%SIZEC];
    while (ptr != NULL) {
        if (ptr->key == key) {
            *pkey = ptr->pkey;
            return ptr;
        }
        ptr = ptr->next;
    }
    return NULL; //при ненахождении возвращает нуль
}

struct key *findk(struct key *key[], int pk) { //поиск указанного элемента в таблице ключей(возвращает сам элемент)
    struct key *ptr = key[hash(pk)%SIZEP];
    while (ptr != NULL) {
        if (ptr->pk == pk)
            return ptr;
        ptr = ptr->next;
    }
    return NULL; //при ненахождении возвращает нуль
}

int deletep(struct parent *par[], int key) { //удаление элемента родителской таюлицы
    struct parent *ptr = par[hash(key)%SIZEP];
    struct parent *tmp;
    if (ptr == NULL)
        return 2;
    if (ptr->key == key) {
        par[hash(ptr->key)%SIZEP] = ptr->next;
        free(ptr->info);
        free(ptr);
        return 0;
    }
    while (ptr->next != NULL || ptr->next->key != key) {
        ptr = ptr->next; //поиск ключа
    }
    if (ptr->next == NULL)
        return 2; //заданный ключ не был найден
    tmp = ptr->next;
    ptr->next = tmp->next;
    free(tmp->info);
    free(tmp);
    return 0;
}

int deletec(struct child *ch[], int key, int *pk) { //удаление элемента детской таблицы
    struct child *ptr = ch[hash(key)%SIZEC];
    struct child *tmp;
    if (ptr == NULL)
        return 2;
    if (ptr->key == key) {
        ch[hash(key)%SIZEC] = ptr->next;
        *pk = ptr->pkey;
        free(ptr->info);
        free(ptr);
        return 0;
    }
    while (ptr->next != NULL || ptr->next->key != key) { //поиск в таблице
        ptr = ptr->next;
    }
    if (ptr->next == NULL)
        return 2;//элемент не найден
    tmp = ptr->next;
    ptr->next = tmp->next;
    *pk = tmp->pkey;
    free(tmp->info);
    free(tmp);
    return 0;
}

void deletek(struct key *key[], int pk) { //удаление элемента в таблице ключей
    struct key *ptr = key[hash(pk)%SIZEP];
    struct key *tmp;
    if (ptr->pk == pk) {
        key[hash(pk)%SIZEP] = ptr->next;
        free(ptr);
        return;
    }
    while (ptr->next->pk != pk) {
        ptr = ptr->next;
    }
    tmp = ptr->next;
    ptr->next = tmp->next;
    free(tmp);
}

int d_addP(struct table *ptab) {
    int k, n;
    char *info;
    printf("Enter key: -->");
    n = getint(&k);
    if(n == 0) {
        return 3;
    }
    if (findp(ptab->par, k) != NULL) {
        return 1;
    }
    printf("Enter info:\n");
    info = getstr();
    if (info == NULL)
        return 4;
    insertp(ptab->par, k, info);
    return 0;
}

void delTable(struct table * tab) {//полная очистка таблицы
    for (int i = 0; i < SIZEP; ++i) {
        struct parent *tmp = NULL;
        struct parent *head = tab->par[i];
        while(head != NULL) {
            tmp = head;
            head = head->next;
            free(tmp->info);
            free(tmp);
        }
    }
    for (int i = 0; i < SIZEP; ++i) {
        struct key *tmp = NULL;
        struct key *head = tab->key[i];
        while(head != NULL) {
            tmp = head;
            head = head->next;
            free(tmp);
        }
    }
    for (int i = 0; i < SIZEC; ++i) {
        struct child *tmp = NULL;
        struct child *head = tab->ch[i];
        while(head != NULL) {
            tmp = head;
            head = head->next;
            free(tmp->info);
            free(tmp);
        }
    }
}

int d_addC(struct table *ptab) {
    int k, pk, n;
    char *info = NULL;
    printf("Enter key: -->");
    n = getint(&k);
    if(n == 0)
        return 3;
    if (findc(ptab->ch, k, &n) != NULL)
        return 1;
    printf("Enter pkey: -->");
    n = getint(&pk);
    if(n == 0)
        return 3;
    if (findp(ptab->par, pk) == NULL)
        return 5;
    printf("Enter info:\n");
    info = getstr();
    if (info == NULL)
        return 4;
    insertc(ptab->ch, k, pk, info);
    insertk(ptab->key, k, pk);
    return 0;
}

int d_findP(struct table *table) {
    int n, k;
    struct parent *par;
    printf("Enter key: -->");
    n = getint(&k);
    if(n == 0)
        return 3;
    par = findp(table->par, k);
    if (par == NULL)
        return 2;
    printf("For key %d: %s\n", k, par->info);
    return 0;
}

int d_findC(struct table *table) {
    int n, k, pk;
    struct child *ch;
    printf("Enter key: -->");
    n = getint(&k);
    if(n == 0)
        return 3;
    ch = findc(table->ch, k, &pk);
    if (ch == NULL)
        return 2;
    printf("For key %d: pkey %d and info %s\n", k, pk, ch->info);
    return 0;
}

int d_deleteP(struct table *table) {
    int n, k;
    printf("Enter key: -->");
    n = getint(&k);
    if (n == 0)
        return 3;
    if (findk(table->key, k) != NULL)
        return 6;
    n = deletep(table->par, k);
    return n;
}

int d_deleteC(struct table *table) {
    int n, k, pk;
    printf("Enter key: -->");
    n = getint(&k);
    if (n == 0)
        return 3;
    n = deletec(table->ch, k, &pk);
    if (!n)
        deletek(table->key, pk);
    return n;
}

int d_showP(struct table *table) {
    for (int i = 0; i < SIZEP; ++i) {
        if (table->par[i] != NULL)
            printlistp(table->par[i]);
    }
    return 0;
}

int d_showC(struct table *table) {
    for (int i = 0; i < SIZEC; ++i) {
        if (table->ch[i] != NULL)
            printlistc(table->ch[i]);
    }
    return 0;
}

int dialog(int N) {
    char *errmsg = "";
    int rc;
    int i, n;
    do{
        puts(errmsg);
        errmsg = "You are wrong. Repeat, please!";
        for(i = 0; i < N; ++i)
            puts(msgs[i]);
        printf("Make your choice: --> ");
        n = getint(&rc);
        if(n == 0)
            rc = 0;
    } while(rc < 0 || rc >= N);
    return rc;
}

int main()
{
    struct table table;
    init(&table);
    int (*fptr[])(struct table *) = {NULL, d_addP, d_addC, d_findP, d_findC, d_deleteP, d_deleteC, d_showP, d_showC};
    int rc;
    while((rc = dialog(NMsgs)))
        printf("%s\n", errmsgs[fptr[rc](&table)]);
    delTable(&table);
    printf("That's all. Bye!\n");
    return 0;
}